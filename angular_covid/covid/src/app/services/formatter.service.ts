import { Injectable } from '@angular/core';
import * as moment from 'moment';

@Injectable({
  providedIn: 'root'
})
export class FormatterService {

  constructor() { }

  number(num: number) {
    return num && num.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
  }

  porpotion(num: number){
    var x = num*100;
    var t = x.toString();
    var result = t.substr(0, 5)+"%";
    return result
  }

  date(dateString: string) {
    let myMoment: moment.Moment = moment(dateString);

    return myMoment.fromNow();
  }

  param(queryObj: { [x: string]: any; apiKey?: string; country?: string; q?: string; page?: number; pageSize?: number; }): string {
    let queryString = [];

    for(let key in queryObj) {
       queryString.push(`${ key }=${ queryObj[key] }`);
    }

    return queryString.join('&');
  }

}
