import { Injectable } from '@angular/core';
import { countryData } from '../pages/country-list/data.country';
import firebase from 'firebase/app';
import { AngularFirestore } from '@angular/fire/firestore';
@Injectable({
  providedIn: 'root'
})
export class ConfigService {
  config: any = {
    summaryApiUrl: 'https://corona.lmao.ninja/v2/all',
    countriesApiUrl: 'https://corona.lmao.ninja/v2/countries/',
    historicalApiUrl: 'https://disease.sh/v3/covid-19/historical/',
  };

  constructor( private firestore: AngularFirestore) { }

  get(key: string): string {
    return this.config[key] || '';
  }

  set(key: string, value: any): void {
    this.config[key] = value;
  }

  addCountryInfo(countryData: countryData) {
    this.firestore.collection("Country Data").add(countryData);
  }
  // This method returns an Observble of the firebase collection which stores news
  getNews(){
    return this.firestore.collection("news").valueChanges();
  }

  // This method returns an Observble of the firebase collection which stores news
  updateCountriesData(data : countryData[]) {
    for(let key in data){ 
      this.firestore.collection("Country Data").doc(data[key].countryName).set(Object.assign({}, data[key]));
    }
  }

  // Returns an Observable of the firebase collection which stores a given country daily data
  getCountries_db(countryName : string){
    return this.firestore.collection("Country Data").doc(countryName).valueChanges();
  }


}
