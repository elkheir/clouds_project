import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-show-news',
  templateUrl: './show-news.component.html',
  styleUrls: ['./show-news.component.scss']
})
export class ShowNewsComponent implements OnInit {

  constructor() { }

  ngOnInit(): void {
  }

  private handleName(news: string): string{
    let news_ = news.toLowerCase().split(" ").join("-").replace("(","").replace(")","").replace(",","").replace("'","");
    return news_;
  }
}
