import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.scss']
})
export class DashboardPageComponent implements OnInit {

  constructor() { }

  ngOnInit(): void {
  }

  private handleName(name: string): string{
    let countr_name = name.toLowerCase().split(" ").join("-").replace("(","").replace(")","").replace(",","").replace("'","");
    return countr_name;
  }
}
