import { Component, OnInit } from '@angular/core';
import { HttpService } from 'src/app/services/http.service';
import { FormatterService } from 'src/app/services/formatter.service';
import { Title } from '@angular/platform-browser';
import { ConfigService } from 'src/app/services/config.service';
import { generalData } from '../../country-list/data.general';
//import { service } from '../../../services/config.service';

@Component({
  selector: 'dashboard-summary',
  templateUrl: './summary.component.html',
  styleUrls: ['./summary.component.scss']
})
export class SummaryComponent implements OnInit {

  globalStats: any;
  dailyData : any[];
  service: any;

  constructor(
    private _http: HttpService,
    private titleService: Title,
    private configService: ConfigService,
    public formatterService: FormatterService,
) { }

  ngOnInit(): void {
    this.titleService.setTitle('COVID 19 - Stats Tracker | Dashboard');
    setTimeout(() => {
      this.fetchGloablStats();
    }, 500);
  }

  fetchGloablStats() {
    let url = this.configService.get('summaryApiUrl');

    return this._http.get(url).subscribe(res => {
      this.globalStats = res;
    });
  }

  refreshData() {
    this.globalStats = undefined;
    setTimeout(() => {
      this.fetchGloablStats();
    }, 500);
  }

  retrieveWorldData(){
    this.service.updateCountriesData().subscribe((data)=>{
      this.dailyData = [];
      // for each JSON data (one per day) we create a new general data object
      for (let key in data){
        let day = new Date();
        this.dailyData.push(new generalData(data[key]['NewConfirmed'], data[key]['TotalConfirmed'], data[key]['NewDeaths'], data[key]['TotalDeaths'], 
        data[key]['NewRecovered'], data[key]['TotalRecovered'], day));
      }
      // Sorting the daily data by number of total cases
      this.dailyData = this.dailyData.sort(function(a,b){
        return b.totalConfirmed - a.totalConfirmed;
      });
      // Adding the right dates
      let i = 0;
      for (let j = 0; j<this.dailyData.length;j++){
        let day = new Date();
        day.setDate(day.getDate()-i)
        this.dailyData[j].date = day;
        i=i+1;
      }
    });
  }
}
