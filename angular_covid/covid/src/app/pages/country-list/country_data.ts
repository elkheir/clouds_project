export class CountryInfo{
    
    name : string;
    totalCases : number;
    todayCases : number;
    totalRecovered : number;
    todayRecovered : number;
    totalDeaths : number;
    todayDeaths : number;
    listHistory : number[];
    flag : string;

    constructor(
        name : string,
        totalCases : number,
        todayCases : number,
        totalRecovered : number,
        todayRecovered : number,
        totalDeaths : number,
        todayDeaths : number,
        listHistory : number[],
        flag : string,){

            this.name = name;
            this.totalCases = totalCases;
            this.todayCases = todayCases;
            this.totalRecovered = totalRecovered;
            this.todayRecovered =todayRecovered;
            this.totalDeaths = totalDeaths;
            this.todayDeaths = todayDeaths
            this.listHistory = listHistory;
            this.flag = flag;
        }
};