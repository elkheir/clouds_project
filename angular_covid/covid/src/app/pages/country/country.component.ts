import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Location } from '@angular/common';
import { Title } from '@angular/platform-browser';

import { FormatterService } from 'src/app/services/formatter.service';
import { ConfigService } from 'src/app/services/config.service';
import { HttpService } from 'src/app/services/http.service';

@Component({
  selector: 'stats-country',
  templateUrl: './country.component.html',
  styleUrls: ['./country.component.scss']
})
export class CountryPageComponent implements OnInit {
  stats: any = [];
  timeline: any = [];
  timeline1: any = [];
  timeline_week: any = [];
  countryName: string;
  table : any[];
  constructor(
    private _http: HttpService,
    private route: ActivatedRoute,
    private _location: Location,
    private titleService: Title,
    private configService: ConfigService,
    public formatterService: FormatterService
  ) { }
   
  ngOnInit(): void {
    this.route.paramMap.subscribe(params => {
      this.countryName = params.get('countryName');
      console.log(this.countryName)
      this.titleService.setTitle('COVID 19 - Stats Tracker | ' + this.countryName);
      this.fetchAll();
      this.fetchHistoricalData();
      this.fetchHistoricalData_week();
      this.fetchin();
      /*this.table.querySelectorAll('th').forEach(item =>{
        console.log(item);
        item.addEventListener('click', function(){
          switch(item.textContent){
            case 'Country ':
              this.updateSortBool(item.textContent);
              if(!this.asc){
                this.countries = this.countries.sort(function(a,b){
                  return a.countryName.localeCompare(b.countryName);
                });
                this.asc = !this.asc;
              }else{
                this.countries = this.countries.sort(function(a,b){
                  return b.countryName.localeCompare(a.countryName);
                });
                this.asc = !this.asc;
              }
              break;
            case 'New Cases ':
              this.updateSortBool(item.textContent);
              if(!this.asc){
                this.countries = this.countries.sort(function(a,b){
                  return a.newConfirmed - b.newConfirmed;
                });
                this.asc = !this.asc;
              }else{
                this.countries = this.countries.sort(function(a,b){
                  return b.newConfirmed - a.newConfirmed;
                });
                this.asc = !this.asc;
              }
              break;
            case 'Total Cases ':
              this.updateSortBool(item.textContent);
              if(!this.asc){
                this.countries = this.countries.sort(function(a,b){
                  return a.totalConfirmed - b.totalConfirmed;
                });
                this.asc = !this.asc;
              }else{
                this.countries = this.countries.sort(function(a,b){
                  return b.totalConfirmed - a.totalConfirmed;
                });
                this.asc = !this.asc;
              }
              break;
            case 'New Recoveries ':
              this.updateSortBool(item.textContent);
              if(!this.asc){
                this.countries = this.countries.sort(function(a,b){
                  return a.newRecovered - b.newRecovered;
                });
                this.asc = !this.asc;
              }else{
                this.countries = this.countries.sort(function(a,b){
                  return b.newRecovered - a.newRecovered;
                });
                this.asc = !this.asc;
              }
              break;
            case 'Total Recoveries ':
              this.updateSortBool(item.textContent);
              if(!this.asc){
                this.countries = this.countries.sort(function(a,b){
                  return a.totalRecovered - b.totalRecovered;
                });
                this.asc = !this.asc;
              }else{
                this.countries = this.countries.sort(function(a,b){
                  return b.totalRecovered - a.totalRecovered;
                });
                this.asc = !this.asc;
              }
              break;
            case 'New Deaths ':
              this.updateSortBool(item.textContent);
              if(!this.asc){
                this.countries = this.countries.sort(function(a,b){
                  return a.newDeaths - b.newDeaths;
                });
                this.asc = !this.asc;
              }else{
                this.countries = this.countries.sort(function(a,b){
                  return b.newDeaths - a.newDeaths;
                });
                this.asc = !this.asc;
              }
              break;
            case 'Total Deaths ':
              this.updateSortBool(item.textContent);
              if(!this.asc){
                this.countries = this.countries.sort(function(a,b){
                  return a.totalDeaths - b.totalDeaths;
                });
                this.asc = !this.asc;
              }else{
                this.countries = this.countries.sort(function(a,b){
                  return b.totalDeaths - a.totalDeaths;
                });
                this.asc = !this.asc;
              }
              break;
            default:
              console.log("didn't work");
          }
        }.bind(this));
      });*/
    });
  }

  fetching(){
    this.table.querySelectorAll('th').forEach(item =>{
      console.log(item);
      item.addEventListener('click', function(){
        switch(item.textContent){
          case 'Country ':
            this.updateSortBool(item.textContent);
            if(!this.asc){
              this.countries = this.countries.sort(function(a,b){
                return a.countryName.localeCompare(b.countryName);
              });
              this.asc = !this.asc;
            }else{
              this.countries = this.countries.sort(function(a,b){
                return b.countryName.localeCompare(a.countryName);
              });
              this.asc = !this.asc;
            }
            break;
          case 'New Cases ':
            this.updateSortBool(item.textContent);
            if(!this.asc){
              this.countries = this.countries.sort(function(a,b){
                return a.newConfirmed - b.newConfirmed;
              });
              this.asc = !this.asc;
            }else{
              this.countries = this.countries.sort(function(a,b){
                return b.newConfirmed - a.newConfirmed;
              });
              this.asc = !this.asc;
            }
            break;
          case 'Total Cases ':
            this.updateSortBool(item.textContent);
            if(!this.asc){
              this.countries = this.countries.sort(function(a,b){
                return a.totalConfirmed - b.totalConfirmed;
              });
              this.asc = !this.asc;
            }else{
              this.countries = this.countries.sort(function(a,b){
                return b.totalConfirmed - a.totalConfirmed;
              });
              this.asc = !this.asc;
            }
            break;
          case 'New Recoveries ':
            this.updateSortBool(item.textContent);
            if(!this.asc){
              this.countries = this.countries.sort(function(a,b){
                return a.newRecovered - b.newRecovered;
              });
              this.asc = !this.asc;
            }else{
              this.countries = this.countries.sort(function(a,b){
                return b.newRecovered - a.newRecovered;
              });
              this.asc = !this.asc;
            }
            break;
          case 'Total Recoveries ':
            this.updateSortBool(item.textContent);
            if(!this.asc){
              this.countries = this.countries.sort(function(a,b){
                return a.totalRecovered - b.totalRecovered;
              });
              this.asc = !this.asc;
            }else{
              this.countries = this.countries.sort(function(a,b){
                return b.totalRecovered - a.totalRecovered;
              });
              this.asc = !this.asc;
            }
            break;
          case 'New Deaths ':
            this.updateSortBool(item.textContent);
            if(!this.asc){
              this.countries = this.countries.sort(function(a,b){
                return a.newDeaths - b.newDeaths;
              });
              this.asc = !this.asc;
            }else{
              this.countries = this.countries.sort(function(a,b){
                return b.newDeaths - a.newDeaths;
              });
              this.asc = !this.asc;
            }
            break;
          case 'Total Deaths ':
            this.updateSortBool(item.textContent);
            if(!this.asc){
              this.countries = this.countries.sort(function(a,b){
                return a.totalDeaths - b.totalDeaths;
              });
              this.asc = !this.asc;
            }else{
              this.countries = this.countries.sort(function(a,b){
                return b.totalDeaths - a.totalDeaths;
              });
              this.asc = !this.asc;
            }
            break;
          default:
            console.log("didn't work");
        }
      }.bind(this));
    });
  }
  
  fetchAll() {
    let url = this.configService.get('countriesApiUrl') + this.countryName;

    return this._http.get(url).subscribe(res => {
      this.stats = res;
    });
  }

  fetchHistoricalData() {
    let url = this.configService.get('historicalApiUrl') + this.countryName+"?lastdays=all";

    return this._http.get(url).subscribe(res => {
      this.timeline = res['timeline'];
    });
  }

  fetchHistoricalData_week() {
    let url = this.configService.get('historicalApiUrl') + this.countryName+"?lastdays=8";
    
    return this._http.get(url).subscribe(res => {
      this.timeline_week = res['timeline'];
    });
  }

  fetchin(){
    let url = this.configService.get('countriesApiUrl') + this.countryName;
    
    return this._http.get(url).subscribe(res => { 
      this.timeline1 = [res['cases'],res['recovered'],res['deaths']];
      console.log(this.timeline1);
    });
  }
  
}


